﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnBoarding.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Security.Claims;
    using System.Web;
    using System.Web.Mvc;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using OnBoarding.Models;

    public class AccountController : Controller
    {
        public AccountController()
        {
        }
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (this.Request.IsAuthenticated)
            {
                return this.RedirectToLocal(returnUrl);
            }
            return this.View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (SuitmediaDbEntities2 db = new SuitmediaDbEntities2())
                    {
                        var obj = db.Logins.Where(a => a.username.Equals(model.Username) && a.password.Equals(model.Password)).FirstOrDefault();
                        //System.Diagnostics.Debug.WriteLine("This will be displayed in output windowdw");
                        //System.Diagnostics.Debug.WriteLine(returnUrl);
                        if (obj != null)
                        {
                            this.SignInUser(obj.username, false);
                            return RedirectToAction(returnUrl);
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Invalid username or password.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
            return this.View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            try
            {
                var ctx = Request.GetOwinContext();
                var authenticationManager = ctx.Authentication;
                // Sign Out.    
                authenticationManager.SignOut();
            }
            catch (Exception ex)
            {
                // Info    
                throw ex;
            }
            //Back to Login Form
            return this.RedirectToAction("Login", "Account");
        }
        private void SignInUser(string username, bool isPersistent)
        {
            // Initialization.    
            var claims = new List<Claim>();
            try
            {
                // Setting    
                claims.Add(new Claim(ClaimTypes.Name, username));
                //claims.Add(new Claim(ClaimTypes.Role, username));
                var claimIdenties = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                var ctx = Request.GetOwinContext();
                var authenticationManager = ctx.Authentication;
                // Sign In.    
                authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, claimIdenties);
            }
            catch (Exception ex)
            {
                // Info    
                throw ex;
            }
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            try
            {
                // Verification.    
                if (Url.IsLocalUrl(returnUrl))
                {
                    // Info.    
                    return this.Redirect(returnUrl);
                }
            }
            catch (Exception ex)
            {
                // Info    
                throw ex;
            }
            // Info.    
            return this.RedirectToAction("Index", "Home");
        }
    }
}